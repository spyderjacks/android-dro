/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.core.Point;
import com.yuriystoys.dro.core.Workspace;
import com.yuriystoys.dro.tools.Tool;

public class Repository {
	public static final String DATABASE_NAME = "DRO_db";

	public static final int FALSE = 0;
	public static final int TRUE = 1;

	public static final String TABLE_WORKSPACE = "Workspace";
	public static final String TABLE_TOOL = "Tool";
	public static final String TABLE_POINT = "Point";
	public static final String TABLE_SETTINGS = "Preference";

	public static final String WORKSPACE_ID = "Id";
	public static final String WORKSPACE_NAME = "Name";
	public static final String WORKSPACE_DESCRIPTION = "Description";
	public static final String WORKSPACE_REFERENCE_ID_1 = "RefPoint1Id";
	public static final String WORKSPACE_REFERENCE_ID_2 = "RefPoint2Id";
	public static final String WORKSPACE_REFERENCE_ID_3 = "RefPoint3Id";
	public static final String WORKSPACE_CREATED_ON = "CreatedOn";
	public static final String WORKSPACE_UPDATED_ON = "UpdatedOn";
	public static final String WORKSPACE_DELETED = "Deleted";
	public static final String WORKSPACE_PREFERENCE_BANK_ID = "PreferenceBankId";

	public static final String POINT_ID = "Id";
	public static final String POINT_WORKSPACE_ID = "WorkspaceId";
	public static final String POINT_TOOL_ID = "ToolId";
	public static final String POINT_NAME = "Name";
	public static final String POINT_DESCRIPTION = "Description";
	public static final String POINT_X = "X";
	public static final String POINT_Y = "Y";
	public static final String POINT_Z = "Z";
	public static final String POINT_CREATED_ON = "CreatedOn";
	public static final String POINT_UPDATED_ON = "UpdatedOn";
	public static final String POINT_HIDE = "hide";

	public static final String PREFERENCE_ID = "Id";
	public static final String PREFERENCE_PREFERENCE_BANK_ID = "PreferenceBankId";
	public static final String PREFERENCE_KEY = "Name";
	public static final String PREFERENCE_VALUE = "Type";

	public static final String TOOL_ID = "Id";
	public static final String TOOL_NAME = "Name";
	public static final String TOOL_DESCRIPTION = "Description";
	public static final String TOOL_TYPE = "Type";
	public static final String TOOL_SHANK_DIA = "ShankDiameter";
	public static final String TOOL_FLUTE_DIA = "FluteDiameter";
	public static final String TOOL_FLUTE_LENGTH = "FluteLength";
	public static final String TOOL_TOOTH_COUNT = "ToothCount";
	public static final String TOOL_X_OFFSET = "XOffset";
	public static final String TOOL_Y_OFFSET = "YOffset";
	public static final String TOOL_Z_OFFSET = "ZOffset";

	private static final int DATABASE_VERSION = 23;

	private static final String WORKSPACE_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_WORKSPACE + " ( "
			+ WORKSPACE_ID + " integer primary key autoincrement, " + WORKSPACE_PREFERENCE_BANK_ID + " integer, "
			+ WORKSPACE_NAME + " varchar(50) not null, " + WORKSPACE_DESCRIPTION + " varchar(250) null, "
			+ WORKSPACE_REFERENCE_ID_1 + " integer null, " + WORKSPACE_REFERENCE_ID_2 + " integer null, "
			+ WORKSPACE_REFERENCE_ID_3 + " integer null, " + WORKSPACE_CREATED_ON + " varchar(20) not null, "
			+ WORKSPACE_UPDATED_ON + " varchar(20) not null," + WORKSPACE_DELETED + " integer not null default (0));";

	private static final String POINT_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_POINT + " ( " + POINT_ID
			+ " integer primary key autoincrement, " + POINT_WORKSPACE_ID + " integer not null, " + POINT_TOOL_ID
			+ " integer null, " + POINT_NAME + " varchar(50) not null, " + POINT_DESCRIPTION + " varchar(250) null, "
			+ POINT_X + " integer not null, " + POINT_Y + " integer not null, " + POINT_Z + " integer null, "
			+ POINT_CREATED_ON + " varchar(20) not null, " + POINT_UPDATED_ON + " varchar(20) not null," + POINT_HIDE
			+ " integer not null default (0)," + " FOREIGN KEY (" + POINT_WORKSPACE_ID + ") REFERENCES "
			+ TABLE_WORKSPACE + " (" + WORKSPACE_ID + ")," + " FOREIGN KEY (" + POINT_TOOL_ID + ") REFERENCES "
			+ TABLE_TOOL + " (" + TOOL_ID + "));";

	private static final String PREFERENCE_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_SETTINGS + " ( "
			+ PREFERENCE_ID + " integer primary key autoincrement, " + PREFERENCE_PREFERENCE_BANK_ID
			+ " integer not null, " + PREFERENCE_KEY + " varchar(50) not null, " + PREFERENCE_VALUE
			+ " varchar(100) not null);";

	private static final String TOOL_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_TOOL + " ( " + TOOL_ID
			+ " integer primary key autoincrement, " + TOOL_NAME + " varchar(50) not null, " + TOOL_DESCRIPTION
			+ " varchar(200) null, " + TOOL_TYPE + " integer not null, " + TOOL_SHANK_DIA + " integer null, "
			+ TOOL_FLUTE_DIA + " integer null, " + TOOL_FLUTE_LENGTH + " integer null, " + TOOL_TOOTH_COUNT
			+ " integer not null, " + TOOL_X_OFFSET + " integer null, " + TOOL_Y_OFFSET + " integer null, "
			+ TOOL_Z_OFFSET + " integer null)";

	private DatabaseHelper dbHelper;

	private final Context context;

	/** Constructor */
	private Repository(Context context) {
		// this.context = context;
		dbHelper = new DatabaseHelper(context);
		this.context = context;
	}

	/**
	 * Opens DB connection
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static Repository open(Context context) throws SQLException {

		return new Repository(context);
	}

	/** Closes a database connection */
	public void close() {
		dbHelper.close();
	}

	private class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context ctx) {
			super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {

			db.execSQL(WORKSPACE_CREATE);
			db.execSQL(POINT_CREATE);
			db.execSQL(PREFERENCE_CREATE);
			db.execSQL(TOOL_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// db.execSQL("DROP TABLE IF EXISTS '" + TABLE_WORKSPACE + "';");
			// db.execSQL("DROP TABLE IF EXISTS '" + TABLE_TOOL + "';");
			// db.execSQL("DROP TABLE IF EXISTS '" + TABLE_POINT + "';");

			if (existsColumnInTable(db, TABLE_TOOL, "DiameterNominal")) {
				// this is the old version of the table, so drop it first
				db.execSQL("DROP TABLE " + TABLE_TOOL);
			}

			onCreate(db);

			if (!existsColumnInTable(db, TABLE_WORKSPACE, WORKSPACE_PREFERENCE_BANK_ID)) {
				String sql = "ALTER TABLE " + TABLE_WORKSPACE + " ADD COLUMN " + WORKSPACE_PREFERENCE_BANK_ID
						+ " INTEGER;";

				db.execSQL(sql);
			}
		}

		private boolean existsColumnInTable(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
			try {
				// query 1 row
				Cursor mCursor = inDatabase.rawQuery("SELECT * FROM " + inTable + " LIMIT 0", null);

				// getColumnIndex gives us the index (0 to ...) of the column -
				// otherwise we get a -1
				if (mCursor.getColumnIndex(columnToCheck) != -1)
					return true;
				else
					return false;

			} catch (Exception Exp) {
				// something went wrong. Missing the database? The table?
				Log.d("... - existsColumnInTable",
						"When checking whether a column exists in the table, an error occurred: " + Exp.getMessage());
				return false;
			}
		}
	}

	public static final SimpleDateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

	// Workspace

	public Workspace getWorkspace(int workspaceId) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = db.query(TABLE_WORKSPACE, null, WORKSPACE_ID + "=?", new String[] { Integer.toString(workspaceId) },
				null, null, null);

		Workspace temp = null;

		if (c != null && c.moveToFirst()) {

			try {
				temp = new Workspace(context, c);
			} catch (ParseException e) {
				e.printStackTrace();
				temp = null;
			}

			c.close();
		}

		db.close();

		return temp;
	}

	public Workspace getWorkspace(int preferenceBankNumber, String workspaceName) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = db.query(TABLE_WORKSPACE, null, WORKSPACE_NAME + "=?", new String[] { workspaceName }, null, null,
				null);

		Workspace temp = null;

		if (c != null && c.moveToFirst()) {

			try {
				temp = new Workspace(context, c);
			} catch (ParseException e) {
				e.printStackTrace();
				temp = null;
			}

			c.close();
		}

		db.close();

		return temp;
	}

	public Workspace getLastWorkspace(int preferenceBankNumber) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = db.query(TABLE_WORKSPACE, null, WORKSPACE_DELETED + "=? AND " + WORKSPACE_PREFERENCE_BANK_ID + "=?",
				new String[] { Integer.toString(0), Integer.toString(preferenceBankNumber) }, null, null, null, null);

		Workspace temp = null;

		if (c != null && c.moveToFirst()) {

			try {
				temp = new Workspace(context, c);
			} catch (ParseException e) {
				e.printStackTrace();
				temp = null;
			}

			c.close();
		}

		db.close();

		return temp;
	}

	public List<Workspace> getWorkspacesLight(int preferenceBankNumber) {
		ArrayList<Workspace> output = new ArrayList<Workspace>();

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = db.query(TABLE_WORKSPACE, null, WORKSPACE_DELETED + "=? AND " + WORKSPACE_PREFERENCE_BANK_ID + "=?",
				new String[] { Integer.toString(0), Integer.toString(preferenceBankNumber) }, null, null,
				WORKSPACE_UPDATED_ON);

		if (c != null) {
			while (c.moveToNext()) {
				try {
					Workspace temp = new Workspace(context, c);

					output.add(temp);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			c.close();
		}

		return output;
	}

	public int insertWorkspace(Workspace workspace) {

		int workspaceId = 0;

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues insertValues = new ContentValues();

		insertValues.put(WORKSPACE_PREFERENCE_BANK_ID, workspace.getBankId());
		insertValues.put(WORKSPACE_NAME, workspace.getName());
		insertValues.put(WORKSPACE_DESCRIPTION, workspace.getDescription());

		Date currentDate = new Date();

		insertValues.put(WORKSPACE_CREATED_ON, iso8601Format.format(currentDate));
		insertValues.put(WORKSPACE_UPDATED_ON, iso8601Format.format(currentDate));
		insertValues.put(WORKSPACE_DELETED, 0);

		try {
			db.beginTransaction();

			long rowId = db.insertOrThrow(TABLE_WORKSPACE, null, insertValues);

			if (rowId > 0) {
				db.setTransactionSuccessful();
				workspaceId = (int) rowId;
			}
		} finally {
			db.endTransaction();
			db.close();
		}

		workspace.setId(workspaceId);

		return workspaceId;
	}

	public void update(Workspace workspace) {

		Date currentDate = new Date();

		ContentValues values = new ContentValues();

		values.put(WORKSPACE_UPDATED_ON, iso8601Format.format(currentDate));
		values.put(WORKSPACE_NAME, workspace.getName());
		values.put(WORKSPACE_DESCRIPTION, workspace.getDescription());

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		try {
			db.beginTransaction();

			int updatedRows = db.update(TABLE_WORKSPACE, values, WORKSPACE_ID + "=?",
					new String[] { Integer.toString(workspace.getId()) });

			if (updatedRows > 0) {
				db.setTransactionSuccessful();
			}

			workspace.setUpdatedOn(currentDate);
		} finally {
			db.endTransaction();
			db.close();
		}
	}

	public void delete(Workspace workspace) throws IllegalStateException {

		if (getWorkspaceCount(workspace.getBankId()) <= 1)
			throw new IllegalStateException("The last workspace can't be deleted.");

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		try {
			db.beginTransaction();

			this.deletePoints(workspace.getId(), db);

			int deletedRows = db.delete(TABLE_WORKSPACE, WORKSPACE_ID + "=?",
					new String[] { Integer.toString(workspace.getId()) });

			if (deletedRows > 0) {
				db.setTransactionSuccessful();
			}
		} finally {
			db.endTransaction();
			db.close();
		}
	}

	public void clear(Workspace workspace) throws IllegalStateException {

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		try {
			db.beginTransaction();

			this.deletePoints(workspace.getId(), db);

			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
			db.close();
		}
	}

	private void deletePoints(int workspaceId, SQLiteDatabase db) {
		db.delete(TABLE_POINT, POINT_WORKSPACE_ID + "=?", new String[] { Integer.toString(workspaceId) });
	}

	public long getWorkspaceCount(int bankId) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();

		try {
			SQLiteStatement statement = db.compileStatement("SELECT COUNT(" + WORKSPACE_ID + ") FROM "
					+ TABLE_WORKSPACE + " WHERE " + WORKSPACE_PREFERENCE_BANK_ID + "=?");

			statement.bindString(1, Integer.toString(bankId));

			return statement.simpleQueryForLong();
		} finally {
			db.close();
		}
	}

	// Point

	public Point getPoint(int pointId) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = db.query(TABLE_POINT, null, POINT_ID + "=?", new String[] { Integer.toString(pointId) }, null, null,
				null);

		Point temp = null;

		if (c != null) {
			c.moveToFirst();

			try {

				temp = new Point(c);
			} catch (ParseException e) {
				e.printStackTrace();
				temp = null;
			}

			c.close();
		}

		db.close();

		return temp;
	}

	public List<Point> getPoints(Integer workspaceId) {

		List<Point> points = new ArrayList<Point>();

		if (workspaceId == null)
			return points;

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = db.query(TABLE_POINT, null, POINT_WORKSPACE_ID + "=? AND " + POINT_HIDE + "=?", new String[] {
				Integer.toString(workspaceId), Integer.toString(FALSE) }, null, null, null);

		if (c != null) {

			while (c.moveToNext()) {
				try {
					points.add(new Point(c));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			c.close();
		}

		db.close();

		return points;
	}

	/**
	 * @param workspaceId
	 * @param name
	 *            point name
	 * @param description
	 *            (optional) point description
	 * @param rawPositionX
	 *            raw X position
	 * @param rawPositionY
	 *            raw Y position
	 * @param rawPositionZ
	 *            raw Z position (optional)
	 * @return ID of the newly inserted point
	 */
	public int insertPoint(int workspaceId, String name, String description, int rawPositionX, int rawPositionY,
			Integer rawPositionZ) {
		int pointId = 0;

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues insertValues = new ContentValues();
		insertValues.put(POINT_WORKSPACE_ID, workspaceId);
		insertValues.put(POINT_NAME, name);
		insertValues.put(POINT_DESCRIPTION, description);

		insertValues.put(POINT_X, rawPositionX);
		insertValues.put(POINT_Y, rawPositionY);
		insertValues.put(POINT_Z, rawPositionZ);

		Date currentDate = new Date();

		insertValues.put(POINT_CREATED_ON, iso8601Format.format(currentDate));
		insertValues.put(POINT_UPDATED_ON, iso8601Format.format(currentDate));

		try {
			db.beginTransaction();

			long rowId = db.insertOrThrow(TABLE_POINT, null, insertValues);

			if (rowId > 0) {
				db.setTransactionSuccessful();
				pointId = (int) rowId;
			}
		} finally {
			db.endTransaction();
			db.close();
		}

		return pointId;
	}

	public int insertPoint(int workspaceId, Point point) {
		int pointId = 0;
		String pointName = point.getName();
		SQLiteDatabase db = null;

		// if point name not passes, make one
		if (pointName == null || pointName.trim().length() == 0) {
			db = dbHelper.getReadableDatabase();
			try {
				long count = DatabaseUtils.queryNumEntries(db, TABLE_POINT, POINT_WORKSPACE_ID + "=?",
						new String[] { Integer.toString(workspaceId) });

				pointName = String.format(Locale.US, "Point %d", count + 1);

			} finally {
				db.close();
			}
		}

		db = dbHelper.getWritableDatabase();

		ContentValues insertValues = new ContentValues();
		insertValues.put(POINT_WORKSPACE_ID, workspaceId);
		insertValues.put(POINT_NAME, pointName);
		insertValues.put(POINT_DESCRIPTION, point.getDescription());

		insertValues.put(POINT_X, point.getX());
		insertValues.put(POINT_Y, point.getY());
		insertValues.put(POINT_Z, point.getZ());

		Date currentDate = new Date();

		insertValues.put(POINT_CREATED_ON, iso8601Format.format(currentDate));
		insertValues.put(POINT_UPDATED_ON, iso8601Format.format(currentDate));

		try {
			db.beginTransaction();

			long rowId = db.insertOrThrow(TABLE_POINT, null, insertValues);

			if (rowId > 0) {
				db.setTransactionSuccessful();
				pointId = (int) rowId;
			}
		} finally {
			db.endTransaction();
			db.close();
		}

		point.setId(pointId);

		return pointId;
	}

	public int update(Point point) {
		int pointId = 0;
		SQLiteDatabase db = null;

		db = dbHelper.getReadableDatabase();
		try {
			long count = DatabaseUtils.queryNumEntries(
					db,
					TABLE_POINT,
					POINT_WORKSPACE_ID + "=? AND " + POINT_ID + "!=? AND " + POINT_NAME + "=?",
					new String[] { Integer.toString(point.getWorkspaceId()), Integer.toString(point.getId()),
							point.getName() });

			if (count > 0)
				throw new IllegalStateException("Point name must be unique within a workspace");

		} finally {
			db.close();
		}

		db = dbHelper.getWritableDatabase();

		ContentValues updateValues = new ContentValues();
		updateValues.put(POINT_NAME, point.getName());
		updateValues.put(POINT_DESCRIPTION, point.getDescription());

		updateValues.put(POINT_X, point.getX());
		updateValues.put(POINT_Y, point.getY());
		updateValues.put(POINT_Z, point.getZ());

		updateValues.put(POINT_UPDATED_ON, iso8601Format.format(new Date()));

		try {
			db.beginTransaction();

			int rowId = db.update(TABLE_POINT, updateValues, POINT_ID + "=?",
					new String[] { Integer.toString(point.getId()) });

			if (rowId > 0) {
				db.setTransactionSuccessful();
			}
		} finally {
			db.endTransaction();
			db.close();
		}

		return pointId;
	}

	public void insertPointGroup(int workspaceId, String groupName, Point[] points) {

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		long count;
		try {
			count = DatabaseUtils.queryNumEntries(db, TABLE_POINT, POINT_WORKSPACE_ID + "=?",
					new String[] { Integer.toString(workspaceId) });

		} finally {
			db.close();
		}

		db = dbHelper.getWritableDatabase();

		for (Point point : points) {
			db = dbHelper.getWritableDatabase();

			ContentValues insertValues = new ContentValues();
			insertValues.put(POINT_WORKSPACE_ID, workspaceId);
			insertValues.put(POINT_NAME, String.format(Locale.US, "%s - %d", groupName, ++count));
			insertValues.put(POINT_DESCRIPTION, point.getDescription());

			insertValues.put(POINT_X, point.getX());
			insertValues.put(POINT_Y, point.getY());
			insertValues.put(POINT_Z, point.getZ());

			Date currentDate = new Date();

			insertValues.put(POINT_CREATED_ON, iso8601Format.format(currentDate));
			insertValues.put(POINT_UPDATED_ON, iso8601Format.format(currentDate));

			try {
				db.beginTransaction();

				long rowId = db.insertOrThrow(TABLE_POINT, null, insertValues);

				if (rowId > 0) {
					db.setTransactionSuccessful();
					point.setId((int) rowId);
				}
			} finally {
				db.endTransaction();
				db.close();
			}
		}
	}

	public void hidePoint(int pointId) {

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues insertValues = new ContentValues();

		insertValues.put(POINT_HIDE, 1);

		Date currentDate = new Date();

		insertValues.put(POINT_UPDATED_ON, iso8601Format.format(currentDate));

		try {

			db.beginTransaction();

			long rowId = db.update(TABLE_POINT, insertValues, POINT_ID + "=?",
					new String[] { Integer.toString(pointId) });

			if (rowId > 0) {
				db.setTransactionSuccessful();
				pointId = (int) rowId;
			}

		} finally {
			db.endTransaction();
			db.close();
		}
	}

	/* Preferences */

	public void setPreference(int bankId, String key, String value) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();
		ContentValues values = new ContentValues();

		values.put(PREFERENCE_PREFERENCE_BANK_ID, bankId);
		values.put(PREFERENCE_KEY, key);
		values.put(PREFERENCE_VALUE, value);

		try {

			db.beginTransaction();

			long rowId = db.update(TABLE_SETTINGS, values, PREFERENCE_PREFERENCE_BANK_ID + "=? and " + PREFERENCE_KEY
					+ "=?", new String[] { Integer.toString(bankId), key.trim() });

			if (rowId == 0) {
				rowId = db.insertOrThrow(TABLE_SETTINGS, null, values);
			}

			if (rowId > 0) {
				db.setTransactionSuccessful();
			}

		} finally {
			db.endTransaction();
			db.close();
		}
	}

	public String getPreference(int bankId, String key) {

		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = db.query(TABLE_SETTINGS, new String[] { PREFERENCE_VALUE }, PREFERENCE_PREFERENCE_BANK_ID
				+ "=? and " + PREFERENCE_KEY + "=? LIMIT 1;", new String[] { Integer.toString(bankId), key.trim() },
				null, null, null, null);

		String temp = null;

		if (c != null && c.moveToFirst()) {

			try {
				temp = c.getString(0);
			} catch (Exception e) {
				e.printStackTrace();
				temp = null;
			}

			c.close();
		}

		db.close();

		return temp;
	}

	/* Tools */

	public int insert(Tool tool) {
		int toolId = 0;
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues insertValues = new ContentValues();

		insertValues.put(TOOL_NAME, tool.getName());
		insertValues.put(TOOL_DESCRIPTION, tool.getDescription());
		insertValues.put(TOOL_TYPE, tool.getType().ordinal());
		insertValues.put(TOOL_SHANK_DIA, (int) (tool.getShankDiameter() * Tool.FACTOR));
		insertValues.put(TOOL_FLUTE_DIA, (int) (tool.getFluteDiameter() * Tool.FACTOR));
		insertValues.put(TOOL_FLUTE_LENGTH, (int) (tool.getFluteLength() * Tool.FACTOR));
		insertValues.put(TOOL_TOOTH_COUNT, tool.getToothCount());
		insertValues.put(TOOL_X_OFFSET, (int) (tool.getOffsetX() * Tool.FACTOR));
		insertValues.put(TOOL_Y_OFFSET, (int) (tool.getOffsetY() * Tool.FACTOR));
		insertValues.put(TOOL_Z_OFFSET, (int) (tool.getOffsetZ() * Tool.FACTOR));

		try {
			db.beginTransaction();

			long rowId = db.insertOrThrow(TABLE_TOOL, null, insertValues);

			if (rowId > 0) {
				db.setTransactionSuccessful();
				toolId = (int) rowId;
			}
		} finally {
			db.endTransaction();
			db.close();
		}

		tool.setId(toolId);

		return toolId;
	}

	public boolean update(Tool tool) {
		ContentValues values = new ContentValues();

		values.put(TOOL_NAME, tool.getName());
		values.put(TOOL_DESCRIPTION, tool.getDescription());
		values.put(TOOL_TYPE, tool.getType().ordinal());
		values.put(TOOL_SHANK_DIA, tool.getShankDiameter() * Tool.FACTOR);
		values.put(TOOL_FLUTE_DIA, tool.getFluteDiameter() * Tool.FACTOR);
		values.put(TOOL_FLUTE_LENGTH, tool.getFluteLength() * Tool.FACTOR);
		values.put(TOOL_TOOTH_COUNT, tool.getToothCount());
		values.put(TOOL_X_OFFSET, tool.getOffsetX() * Tool.FACTOR);
		values.put(TOOL_Y_OFFSET, tool.getOffsetY() * Tool.FACTOR);
		values.put(TOOL_Z_OFFSET, tool.getOffsetZ() * Tool.FACTOR);

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		try {
			db.beginTransaction();

			int updatedRows = db.update(TABLE_TOOL, values, TOOL_ID + "=?",
					new String[] { Integer.toString(tool.getId()) });

			if (updatedRows > 0) {
				db.setTransactionSuccessful();
			}

			return updatedRows > 0;

		} finally {
			db.endTransaction();
			db.close();
		}
	}

	public Tool getTool(int toolId) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor c = db.query(TABLE_TOOL, null, TOOL_ID + "=?", new String[] { Integer.toString(toolId) }, null, null,
				null);

		Tool temp = null;

		if (c != null && c.moveToFirst()) {

			try {

				temp = new Tool(c);
			} catch (ParseException e) {
				e.printStackTrace();
				temp = null;
			}

			c.close();
		}

		db.close();

		return temp;
	}

	public List<Tool> getTools(MachineTypes machineType) {
		List<Tool> tools = new ArrayList<Tool>();

		SQLiteDatabase db = dbHelper.getReadableDatabase();

		Cursor c = db.query(TABLE_TOOL, null, TOOL_TYPE + "=?",
				new String[] { Integer.toString(machineType.ordinal()) }, null, null, null);

		if (c != null) {

			while (c.moveToNext()) {
				try {
					tools.add(new Tool(c));
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			c.close();
		}

		db.close();

		return tools;
	}

	public void deleteTool(int toolId) {

		SQLiteDatabase db = dbHelper.getWritableDatabase();

		try {
			db.beginTransaction();

			int deletedRows = db.delete(TABLE_TOOL, TOOL_ID + "=?", new String[] { Integer.toString(toolId) });

			if (deletedRows > 0) {
				db.setTransactionSuccessful();
			}
		} finally {
			db.endTransaction();
			db.close();
		}
	}
}
