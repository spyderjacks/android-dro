package com.yuriystoys.dro.functions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.ui.ToolOffsetLatheDialog;

public class LatheToolOffset extends IDroFunction {

	private final View button;

	public LatheToolOffset(Activity activity, ViewGroup target) {
		super(activity, target, MachineTypes.LATHE);

		View view = activity.getLayoutInflater().inflate(R.layout._button_tool_offset_lathe, target);

		button = view.findViewById(R.id.toolOffsetButton);

		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				ToolOffsetLatheDialog.Show(LatheToolOffset.this.activity);
			}
		});

		button.setOnLongClickListener(new OnLongClickListener() {

			@SuppressLint("ValidFragment") @Override
			public boolean onLongClick(View v) {

				DialogFragment dialog = new DialogFragment() {

					@Override
					public Dialog onCreateDialog(Bundle savedInstanceState) {
						AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

						builder.setTitle(R.string.tool_ofset_clear_title).setMessage(R.string.tool_ofset_clear_descr)
								.setPositiveButton(R.string.yes_button, new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										
										Dro dro = ((DroApplication) getActivity().getApplication()).getDro();
										
										dro.getAxis(Axis.X).clearToolOffset();
										dro.getAxis(Axis.Z).clearToolOffset();
										dro.clearSelectedToolId();
									}
								}).setNegativeButton(R.string.no_button, null);
						// Create the AlertDialog object and return it
						return builder.create();
					}

				};
				dialog.show(((FragmentActivity) LatheToolOffset.this.activity).getSupportFragmentManager(), "Dialog");

				return false;
			}
		});
	}

	@Override
	public void showForMachine(MachineTypes type) {
		button.setVisibility(type == this.type ? View.VISIBLE : View.GONE);
	}
}
