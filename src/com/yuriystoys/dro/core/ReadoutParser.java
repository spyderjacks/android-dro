/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.core;

import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.callbacks.IReadoutCallback;

/**
 * 
 * @author Yuriy Krushelnytskiy
 * 
 * This is the main DRO state machine. It's role is to parse the input one character at a time and 
 * once a reading is constructed, call the callback method to pass the value to the next stage
 *
 */
public class ReadoutParser {
	
    int axis = Axis.NONE;
    int position = 0;
    int multiplier = 1;
    ParserState state = ParserState.Axis_Label;
	final IReadoutCallback  listenter;

    public ReadoutParser(IReadoutCallback listener)
    {
    	this.listenter = listener;
    }
    
	/**
	 * 
	 * @param value
	 * @return Returns true if the input is accepted; false otherwise.
	 */
	public boolean Accept(char value)
	{
		switch (state)
        {
            case Axis_Label:
                {
                    int axis = ParseAxis(value);

                    if (axis > Axis.NONE)
                    {
                        this.axis = axis;
                        state = ParserState.DigitOrSign;
                        return true;
                    }

                    break;
                }
            case DigitOrSign:
                {
                    //first thing after the axis can be a sign or a digit


                    if (value == '-')
                    {
                        multiplier = -1;
                        state = ParserState.Digit0;
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = value - '0';
                        state = ParserState.Digit1;
                        return true;
                    }

                    break;
                }
            case Digit0:
                {
                    //this has to be a digit
                    if (Character.isDigit(value))
                    {
                        position = value - '0';
                        state = ParserState.Digit1;
                        return true;
                    }

                    break;
                }
            case Digit1:
                {
                    //this can be a digit or a terminator

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = position * 10 + (value - '0');
                        state = ParserState.Digit2;
                        return true;
                    }

                    break;
                }
            case Digit2:
                {
                    //this can be a digit or a terminator

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = position * 10 + (value - '0');
                        state = ParserState.Digit3;
                        return true;
                    }

                    break;
                }
            case Digit3:
                {
                    //this can be a digit or a terminator

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = position * 10 + (value - '0');
                        state = ParserState.Digit4;
                        return true;
                    }

                    break;
                }
            case Digit4:
                {
                    //this can be a digit or a terminator

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = position * 10 + (value - '0');
                        state = ParserState.Digit5;
                        return true;
                    }

                    break;
                }
            case Digit5:
                {
                    //this can be a digit or a terminator
                    //this can be a digit or a terminator

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = position * 10 + (value - '0');
                        state = ParserState.Digit6;
                        return true;
                    }

                    break;
                }
            case Digit6:
                {
                    //this can be a digit or a terminator

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = position * 10 + (value - '0');
                        state = ParserState.Digit7;
                        return true;
                    }

                    break;
                }
            case Digit7:
                {
                    //this can be a digit or a terminator

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = position * 10 + (value - '0');
                        state = ParserState.Digit8;
                        return true;
                    }

                    break;
                }
            case Digit8:
                {
                    //this can be a digit or a terminator

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = position * 10 + (value - '0');
                        state = ParserState.Digit9;
                        return true;
                    }

                    break;
                }
            case Digit9:
                {
                    //this can be a digit or a terminator

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    if (Character.isDigit(value))
                    {
                        position = position * 10 + (value - '0');
                        state = ParserState.Terminator;
                        return true;
                    }

                    break;
                }
            case Terminator:
                {
                    //write out the data;

                    if (AcceptTerminator(value))
                    {
                        this.listenter.processReadout(axis, position * multiplier);
                        ResetStateMachine();
                        return true;
                    }

                    break;
                }
        }

        return false;
	}
	
    private int ParseAxis(char value)
    {
        switch (value)
        {
            case 'x':
            case 'X':
                {
                    return Axis.X;
                }
            case 'y':
            case 'Y':
                {
                    return Axis.Y;
                }
            case 'z':
            case 'Z':
                {
                    return Axis.Z;
                }
            case 'u':
            case 'U':
                {
                    return Axis.U;
                }
            case 'v':
            case 'V':
                {
                    return Axis.V;
                }
            case 'w':
            case 'W':
                {
                    return Axis.W;
                }
            case 'r':
            case 'R':
                {
                    return Axis.R;
                }
            case 's':
            case 'S':
                {
                    return Axis.S;
                }
            case 't':
            case 'T':
                {
                    return Axis.T;
                }
            case 'a':
            case 'A':
                {
                    return Axis.A;
                }
            case 'b':
            case 'B':
                {
                    return Axis.B;
                }
            case 'c':
            case 'C':
                {
                    return Axis.C;
                }
            default:
                {
                    return Axis.NONE;
                }
        }
    }
    
    private boolean AcceptTerminator(char value)
    {
        return value == ';';
    }


    private void ResetStateMachine()
    {
        axis = Axis.NONE;
        position = 0;
        multiplier = 1;
        state = ParserState.Axis_Label;
    }

}
