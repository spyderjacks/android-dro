package com.yuriystoys.dro.core;

public class ConnectionState {
	// Constants that indicate the current connection state
	public final static int UNKNOWN = -1;
	public final static int UNSUPPORTED = 0;
	public final static int DISABLED = 1;
	public final static int NO_SERVICE = 2;
	public final static int CONNECTION_LOST = 3;
	public final static int DISCONNECTED = 4;
	public final static int CONNECTING = 5;
	public final static int CONNECTED = 6;
}
