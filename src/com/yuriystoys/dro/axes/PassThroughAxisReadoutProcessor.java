package com.yuriystoys.dro.axes;

public class PassThroughAxisReadoutProcessor extends AxisReadoutProcessor {

	public PassThroughAxisReadoutProcessor(AxisSettings axis) {
		super(axis);
	}

	@Override
	public void accept(int position) {
		axis.setPosition(position);
	}
}
