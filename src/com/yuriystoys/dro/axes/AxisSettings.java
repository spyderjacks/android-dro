package com.yuriystoys.dro.axes;

import java.util.ArrayList;
import java.util.List;

import com.yuriystoys.dro.axes.Axis.AxisType;
import com.yuriystoys.dro.callbacks.IModeChangedCallback;
import com.yuriystoys.dro.callbacks.IPositionChangedCallback;
import com.yuriystoys.dro.callbacks.IUnitsChangedCallback;
import com.yuriystoys.dro.core.Dro;

public class AxisSettings {

	private final int axis;
	private final AxisType type;

	private String label;

	private boolean relativeMode;
	private boolean inMetricMode;

	private int workspaceOffset; // offset in relation to the machine origin
	private int incrementalOffset; // offset in relation to the workspace origin
	private int toolOffset; // tool offset

	private int position = 0;
	private int secondaryPostion = 0;

	private int readoutTarget;
	// private double countsPerInch;

	private double countsPerInch;

	// This is the inverse of TPI. This way there is no division every time
	private double inchPerCount;
	private double mmPertCount;
	private double scale = 1D;

	private boolean enabled;
	private boolean inverted;
	private boolean average;

	private AxisReadoutProcessor processor;

	/********* Getters ********/

	/**
	 * Current position in relation to the machine origin
	 * 
	 * @return
	 */
	public int getRawPosition() {
		return position + secondaryPostion;
	}

	/**
	 * Current position in relation to the workspace origin
	 * 
	 * @return
	 */
	public int getAbsolutePosition() {
		return position + secondaryPostion + workspaceOffset;
	}

	/**
	 * Current position in relation to the incremental origin
	 * 
	 * @return
	 */
	public int getIncrementalPosition() {
		return getAbsolutePosition() + incrementalOffset + toolOffset;
	}

	/**
	 * Convert the passed counts to a position relative to the incremental
	 * origin
	 * 
	 * @return
	 */
	public int getIncrementalPosition(int count) {
		return count + incrementalOffset + toolOffset;
	}

	/**
	 * Current position taking into account the selected mode (incr/abs)
	 * 
	 * @return
	 */
	public int getPosition() {
		return this.relativeMode ? getIncrementalPosition() : getAbsolutePosition();
	}

	/**
	 * Difference between the provided counts and the current absolute position
	 * 
	 * @param counts
	 * @return
	 */
	public int getDelta(int counts) {
		return counts - getAbsolutePosition();
	}

	/**
	 * Current readout in relation to the workspace origin
	 * 
	 * @return
	 */
	public double getAbsoluteReadout() {
		return this.inMetricMode ? convertToMm(getAbsolutePosition()) : convertToInches(getAbsolutePosition());
	}

	/**
	 * Current readout in relation to the incremental origin
	 * 
	 * @return
	 */
	public double getIncrementalReadout() {
		return this.inMetricMode ? convertToMm(getIncrementalPosition()) : convertToInches(getIncrementalPosition());
	}

	/**
	 * Current readout taking into account the selected mode (incr/abs) and
	 * units
	 * 
	 * @return
	 */
	public double getReadout() {
		return this.inMetricMode ? convertToMm(getPosition()) : convertToInches(getPosition());
	}

	/**
	 * Offset of the incremental origin in relation to the workspace origin
	 * 
	 * @return
	 */
	public int getIncrementalOffset() {
		return incrementalOffset;
	}

	public int getAxis() {
		return axis;
	}

	public AxisType getAxisType() {
		return type;
	}

	public boolean isInIncrementalMode() {
		return relativeMode;
	}

	public boolean isInMetricMode() {
		return inMetricMode;
	}

	public int getReadoutTarget() {
		return readoutTarget;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isInverted() {
		return this.inverted;
	}

	public boolean hasToolOffset() {
		return toolOffset != 0;
	}

	public String getLabel() {
		return this.label;
	}

	protected double getCountsPerInch() {
		return this.countsPerInch * scale;
	}

	/********* Setters ********/

	public void setLabel(String label) {
		this.label = label;
	}

	public void setCountsPerInch(double countsPerInch) {
		this.countsPerInch = countsPerInch;
		this.inchPerCount = 1 / countsPerInch;
		this.mmPertCount = Dro.MM_PER_INCH * inchPerCount;

		raiseOnPositionChanged();
	}
	
	public void setScale(double scale)
	{
		if(scale==0)
			return;
		
		this.scale = scale;
		raiseOnPositionChanged();
	}
	
	public void clearScale()
	{
		this.scale = 1;
		raiseOnPositionChanged();
	}

	public void setInverted(boolean inverted) {
		this.inverted = inverted;
	}

	public void setAverage(boolean average) {
		this.average = average;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setProcessor(AxisReadoutProcessor processor) {
		this.processor = processor;
	}

	/******* "Convenience" Setters *******/

	/**
	 * Set current position as workspace origin
	 */
	public void setWorkspaceOrigin() {
		incrementalOffset = 0;
		workspaceOffset = -(position + secondaryPostion);

		raiseOnPositionChanged();
	}

	/**
	 * Set current position as incremental origin
	 */
	public void setIncrementalOrigin() {
		incrementalOffset = -getAbsolutePosition();

		raiseOnPositionChanged();
	}

	/**
	 * Set current dimension as incremental offset
	 */
	public void setOffset(int dimension) {
		incrementalOffset = dimension;

		raiseOnPositionChanged();
	}

	/**
	 * Set current dimension as incremental offset
	 */
	public void setOffset(double dimension) {
		incrementalOffset = convertToCounts(dimension);

		raiseOnPositionChanged();
	}

	/**
	 * Set current dimension as incremental offset
	 */
	public void setAbsoluteOffset(int dimension) {
		workspaceOffset = dimension;

		raiseOnPositionChanged();
	}

	/**
	 * Set current dimension as incremental offset
	 */
	public void setAbsoluteOffset(double dimension) {
		workspaceOffset = convertToCounts(dimension);

		raiseOnPositionChanged();
	}

	public void clearOffset() {
		incrementalOffset = 0;

		raiseOnPositionChanged();
	}

	public void setToolOffset(double dimension) {
		toolOffset = this.convertToCounts(dimension);
		setIncrementalMode();
		raiseOnPositionChanged();
	}

	public void clearToolOffset() {
		toolOffset = 0;
		setIncrementalMode();
		raiseOnPositionChanged();
	}

	public void setIncrementalMode() {
		relativeMode = true;

		raiseOnModeChanged();
	}

	public void setAbsoluteMode() {
		relativeMode = false;

		raiseOnModeChanged();
	}

	public void toggleMode() {
		relativeMode = !relativeMode;

		raiseOnModeChanged();
	}

	public void setMetricUnits(boolean metric) {
		if (axis != Axis.T) {
			inMetricMode = metric;
			raiseOnUnitsChanged();
		}
	}

	public void setMetricUnits() {
		setMetricUnits(true);
	}

	public void setInchUnits() {
		setMetricUnits(false);
	}

	public void toggleUnits() {
		if (axis != Axis.T) {
			inMetricMode = !inMetricMode;
			raiseOnUnitsChanged();
		}
	}

	/*****  *****/

	public AxisSettings(int axis, AxisType type) {
		this.axis = axis;
		this.type = type;
	}

	/*
	 * Converts a dimension from inches or mm to counts, based on the currently
	 * selected units
	 */
	public int convertToCounts(double dimension) {
		return inMetricMode ? (int) (dimension / mmPertCount / scale) : (int) (dimension / inchPerCount / scale);
	}

	/*
	 * Converts a dimension from millimeters to counts
	 */
	public int convertMmToCounts(double dimension) {
		return (int) (dimension / mmPertCount);
	}

	/*
	 * Converts a dimension from inches to counts
	 */
	public int convertInchesToCounts(double dimension) {
		return (int) (dimension / inchPerCount);
	}

	/**
	 * Convert the counts to the proper dimension (inches or mm, based on the
	 * current state)
	 * 
	 * @param counts
	 * @return
	 */
	public double convertToDimension(int counts) {
		return inMetricMode ? convertToMm(counts) : convertToInches(counts);
	}

	public double convertToInches(int counts) {
		return inchPerCount * counts * scale;
	}

	public double convertToMm(int counts) {
		return mmPertCount * counts * scale;
	}

	/**
	 * Updates the position for this axis
	 * 
	 * @param position
	 */
	protected void setPosition(int position) {
		if (average)
			position = getAveraged(position);

		this.position = inverted ? -position : position;

		raiseOnPositionChanged();
	}

	/**
	 * Updates the secondary position for this axis
	 * 
	 * @param position
	 */
	protected void setSecondaryPosition(int position) {
		this.secondaryPostion = inverted ? -position : position;

		raiseOnPositionChanged();
	}

	public void acceptPosition(int position) {
		processor.accept(position);
	}

	public void resetPosition() {
		this.position = 0;
		this.secondaryPostion = 0;
	}

	// number of values to be averaged
	private final static int VALUES_TO_AVG = 5;

	private final int[] lastRead = new int[VALUES_TO_AVG];
	private int headIndex = 0;
	private int sum;

	/**
	 * This function is used to reduce the flicker by averaging last values.
	 * 
	 * @param axis
	 * @param position
	 * @return
	 */
	private int getAveraged(int position) {
		if (headIndex >= VALUES_TO_AVG)
			headIndex = 0;

		lastRead[headIndex] = position * 4;

		sum = 0;
		for (int i : lastRead) {
			sum += i;
		}

		headIndex++;

		return Math.round(sum / VALUES_TO_AVG / 4);
	}

	// Callbacks
	private final List<IPositionChangedCallback> positionChangedCallbacks = new ArrayList<IPositionChangedCallback>();
	private final List<IModeChangedCallback> modeChangedCallbacks = new ArrayList<IModeChangedCallback>();
	private final List<IUnitsChangedCallback> unitsChangedCallbacks = new ArrayList<IUnitsChangedCallback>();

	private void raiseOnPositionChanged() {
		for (IPositionChangedCallback callback : positionChangedCallbacks) {
			callback.onPositionChanged(this);
		}
	}

	private void raiseOnModeChanged() {
		for (IModeChangedCallback callback : modeChangedCallbacks) {
			callback.onModeChanged(this);
		}
	}

	private void raiseOnUnitsChanged() {
		for (IUnitsChangedCallback callback : unitsChangedCallbacks) {
			callback.onUnitsChanged(this);
		}
	}

	public void registerCallback(IPositionChangedCallback callback) {
		if (positionChangedCallbacks.isEmpty()) {
			positionChangedCallbacks.add(callback);
		} else if (!positionChangedCallbacks.contains(callback)) {
			positionChangedCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}

	public void registerCallback(IModeChangedCallback callback) {
		if (!modeChangedCallbacks.contains(callback)) {
			modeChangedCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}

	public void registerCallback(IUnitsChangedCallback callback) {
		if (!unitsChangedCallbacks.contains(callback)) {
			unitsChangedCallbacks.add(callback);
		} else {
			throw new IllegalStateException("This callback is already registered.");
		}
	}

	// Methods to remove callback

	public void removeCallback(IPositionChangedCallback callback) {
		if (positionChangedCallbacks.contains(callback)) {
			positionChangedCallbacks.remove(callback);
		} else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	public void removeCallback(IModeChangedCallback callback) {
		if (modeChangedCallbacks.contains(callback))
			modeChangedCallbacks.remove(callback);
		else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}

	public void removeCallback(IUnitsChangedCallback callback) {
		if (unitsChangedCallbacks.contains(callback))
			unitsChangedCallbacks.remove(callback);
		else
			throw new IllegalStateException("Attempting to remove a callback that is not registered");
	}
}
