package com.yuriystoys.dro.ui;

import java.util.List;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.PathShape;
import android.view.View;

import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.axes.AxisSettings;
import com.yuriystoys.dro.callbacks.IPointListChangedCallback;
import com.yuriystoys.dro.callbacks.IPositionChangedCallback;
import com.yuriystoys.dro.core.Point;
import com.yuriystoys.dro.core.Workspace;

public class WorkspaceView extends View implements IPointListChangedCallback, IPositionChangedCallback {

	private Coordinate min = new Coordinate();
	private Coordinate max = new Coordinate();
	private Coordinate origin = new Coordinate();
	private Coordinate position = new Coordinate();
	
	private boolean autoScale = true;
	private float scale = 1F;

	PathShape pathShape;
	ShapeDrawable crossHairs;
	ShapeDrawable circle;

	ShapeDrawable originShape;
	ShapeDrawable cursorShape;

	private Coordinate[] points;

	private static final int POINT_SIZE = 50;
	private static final int PADDING = POINT_SIZE * 2;

	private static final int crossHalfWidth = POINT_SIZE / 2;
	private static final int circleHalfWidth = POINT_SIZE / 4;

	public WorkspaceView(Context context) {
		super(context);

		/* Init point shape */

		Path pointPath = new Path();
		pointPath.moveTo(POINT_SIZE / 2, POINT_SIZE);
		pointPath.lineTo(POINT_SIZE / 2, 0);
		pointPath.moveTo(0, POINT_SIZE / 2);
		pointPath.lineTo(POINT_SIZE, POINT_SIZE / 2);
		pointPath.close();

		crossHairs = new ShapeDrawable(new PathShape(pointPath, POINT_SIZE, POINT_SIZE));
		crossHairs.getPaint().setColor(getResources().getColor(android.R.color.white));
		crossHairs.getPaint().setAlpha(200);
		crossHairs.getPaint().setStyle(Paint.Style.STROKE);
		crossHairs.getPaint().setStrokeWidth(3);

		circle = new ShapeDrawable(new OvalShape());
		circle.getPaint().setColor(getResources().getColor(android.R.color.white));
		crossHairs.getPaint().setAlpha(150);
		circle.getPaint().setStyle(Paint.Style.STROKE);

		/* Init the cursorShape */

		Path cursorPath = new Path();
		cursorPath.moveTo(POINT_SIZE, POINT_SIZE * 2);
		cursorPath.lineTo(POINT_SIZE, 0);
		cursorPath.moveTo(0, POINT_SIZE);
		cursorPath.lineTo(POINT_SIZE * 2, POINT_SIZE);
		cursorPath.close();

		cursorShape = new ShapeDrawable(new PathShape(cursorPath, POINT_SIZE * 2, POINT_SIZE * 2));
		cursorShape.getPaint().setColor(getResources().getColor(android.R.color.holo_green_light));
		cursorShape.getPaint().setAlpha(200);
		cursorShape.getPaint().setStyle(Paint.Style.STROKE);

		/* Init the originShape */

		Path originPath = new Path();
		originPath.moveTo(POINT_SIZE, POINT_SIZE * 2);
		originPath.lineTo(POINT_SIZE, 0);
		originPath.lineTo(0, POINT_SIZE);
		originPath.lineTo(POINT_SIZE * 2, POINT_SIZE);

		originShape = new ShapeDrawable(new PathShape(originPath, POINT_SIZE * 2, POINT_SIZE * 2));
		originShape.getPaint().setColor(getResources().getColor(android.R.color.holo_orange_light));
		originShape.getPaint().setAlpha(200);
		originShape.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
	}

	protected void onDraw(Canvas canvas) {

		int width = canvas.getWidth();
		int height = canvas.getHeight();

		min.X = Math.min(position.X, origin.X);
		min.Y = Math.min(position.Y, origin.Y);
		max.X = Math.max(position.X, origin.X);
		max.Y = Math.max(position.Y, origin.Y);

		// min/max ppoint coordinates
		for (Coordinate point : points) {
			min.X = Math.min(point.X, min.X);
			min.Y = Math.min(point.Y, min.Y);
			max.X = Math.max(point.X, max.X);
			max.Y = Math.max(point.Y, max.Y);
		}

		if(autoScale)
		{
			float scaleX = (float) (width - PADDING * 2) / (float) (max.X - min.X);
			float scaleY = (float) (height - PADDING * 2) / (float) (max.Y - min.Y);
	
			scale = Math.min(1F, Math.min(scaleX, scaleY));
		}

		drawOrigin(canvas, (int) ((origin.X - min.X) * scale) + PADDING, height - ((int) ((origin.Y - min.Y) * scale) + PADDING));

		for (Coordinate point : points) {

			drawPoint(canvas, (int) ((point.X - min.X) * scale) + PADDING, height - ((int) (( point.Y - min.Y) * scale) + PADDING));

		}

		drawCursor(canvas, (int) ((position.X - min.X) * scale) + PADDING, height - ((int) ((position.Y - min.Y) * scale) + PADDING));
	}

	private void drawPoint(Canvas canvas, int centerX, int centerY) {
		crossHairs.setBounds(centerX - crossHalfWidth, centerY - crossHalfWidth, centerX + crossHalfWidth, centerY
				+ crossHalfWidth);
		crossHairs.draw(canvas);

		circle.setBounds(centerX - circleHalfWidth, centerY - circleHalfWidth, centerX + circleHalfWidth, centerY
				+ circleHalfWidth);
		circle.draw(canvas);
	}

	private void drawCursor(Canvas canvas, int centerX, int centerY) {
		cursorShape.setBounds(centerX - POINT_SIZE, centerY - POINT_SIZE, centerX + POINT_SIZE, centerY + POINT_SIZE);
		cursorShape.draw(canvas);
	}

	private void drawOrigin(Canvas canvas, int centerX, int centerY) {
		originShape.setBounds(centerX - crossHalfWidth, centerY - crossHalfWidth, centerX + crossHalfWidth, centerY
				+ crossHalfWidth);
		originShape.draw(canvas);
	}

	public void setWorkspace(Workspace workspace) {
		onPointListChanged(workspace);
	}

	public void setOrigin(int x, int y) {
		origin.X = x;
		origin.Y = y;
		this.invalidate();
	}

	private class Coordinate {
		int X;
		int Y;
	}

	@Override
	public void onPositionChanged(AxisSettings sender) {
		switch(sender.getAxis())
		{
		case Axis.X:
		{
			position.X = sender.getAbsolutePosition();
			this.invalidate();
			break;
		}
		case Axis.Y:
		{
			position.Y = sender.getAbsolutePosition();
			this.invalidate();
			break;
		}
		}
	}

	@Override
	public void onPointListChanged(Workspace sender) {

		List<Point> temp = sender.getPoints();
		int size = temp.size();
		points = new Coordinate[size];

		for (int i = 0; i < size; i++) {
			Point p = temp.get(i);
			Coordinate c = new Coordinate();
			c.X = p.getX();
			c.Y = p.getY();
			points[i] = c;
		}
		
		this.invalidate();
	}
}
