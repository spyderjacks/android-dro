package com.yuriystoys.dro.ui.util;

import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.axes.AxisSettings;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.Dro.ReadoutFormat;
import com.yuriystoys.dro.core.Point;
import com.yuriystoys.dro.core.Workspace;

public class PointsAdapter extends ArrayAdapter<PointsAdapter.PointViewModel> {
	int layoutResourceId;

	final Dro readout;

	public PointsAdapter(Context context, Dro dro, int layoutResourceId, List<PointViewModel> data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.readout = dro;
	}

	public void refresh(Workspace workspace) {
		super.clear();

		List<Point> source = workspace.getPoints();
		int selectedPointId = workspace.getSelectedPointId();

		for (Point point : source)
			super.add(new PointsAdapter.PointViewModel(point, point.getId().equals(selectedPointId)));

		this.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		return getDropDownView(position, convertView, parent);
	}

	Context context;
	final static String coordFormatMm = "";
	final static String coordFormatInch = "";

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {

		View row = convertView;
		PointViewModel point = super.getItem(position);

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
		}

		TextView title = ((TextView) row.findViewById(R.id.title_text_view));
		TextView descr = ((TextView) row.findViewById(R.id.description_text_view));
		TextView coord = ((TextView) row.findViewById(R.id.coordinate_text_view));

		if (point.selected) {
			title.setTypeface(null, Typeface.BOLD);
			title.setTextColor(parent.getContext().getResources().getColor(android.R.color.holo_green_light));
			descr.setTextColor(parent.getContext().getResources().getColor(android.R.color.holo_green_light));
			coord.setTextColor(parent.getContext().getResources().getColor(android.R.color.holo_green_light));
		} else {
			title.setTypeface(null, Typeface.NORMAL);
			title.setTextColor(parent.getContext().getResources().getColor(android.R.color.primary_text_dark));
			descr.setTextColor(parent.getContext().getResources().getColor(android.R.color.primary_text_dark));
			coord.setTextColor(parent.getContext().getResources().getColor(android.R.color.primary_text_dark));
		}

		title.setText(point.name);

		if (point.description != null) {
			descr.setText(point.description);
			descr.setVisibility(View.VISIBLE);
		} else {
			descr.setVisibility(View.GONE);
		}

		coord.setText(point.toString(readout));

		return row;
	}

	public class PointViewModel {
		public int id;
		public String name;
		public String description;
		public int x;
		public int y;
		public Integer z;
		public boolean selected;

		public PointViewModel(Point point, boolean selected) {

			id = point.getId();
			name = point.getName();
			description = point.getDescription();
			this.selected = selected;

			x = point.getX();
			y = point.getY();
			z = point.getZ();
		}

		@SuppressLint("DefaultLocale")
		public String toString(Dro dro) {

			ReadoutFormat format = dro.getCurrentFormat();

			AxisSettings xAxis = dro.getAxis(Axis.X);
			AxisSettings yAxis = dro.getAxis(Axis.Y);
			AxisSettings zAxis = dro.getAxis(Axis.Z);

			if (dro.getMachineType() != MachineTypes.LATHE) {
				if (this.z != null) {
					return String.format(Locale.US, "x:%s, y:%s, z:%s",
							format.format(xAxis.convertToDimension(xAxis.getIncrementalPosition(this.x))),
							format.format(yAxis.convertToDimension(yAxis.getIncrementalPosition(this.y))),
							format.format(zAxis.convertToDimension(zAxis.getIncrementalPosition(this.z))));
				} else {
					return String.format(Locale.US, "x:%s, y:%s",
							format.format(xAxis.convertToDimension(xAxis.getIncrementalPosition(this.x))),
							format.format(yAxis.convertToDimension(yAxis.getIncrementalPosition(this.y))));
				}
			} else {
				if (this.z != null) {
					return String.format(Locale.US, "x:%s, z:%s",
							format.format(xAxis.convertToDimension(xAxis.getIncrementalPosition(this.x))),
							format.format(zAxis.convertToDimension(zAxis.getIncrementalPosition(this.z))));
				}
				else
				{
					return String.format(Locale.US, "x:%s, z:not set",
							format.format(xAxis.convertToDimension(xAxis.getIncrementalPosition(this.x))));
				}
			}
		}
	}
}
