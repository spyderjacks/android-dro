package com.yuriystoys.dro.ui.util;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.yuriystoys.dro.R;
import com.yuriystoys.dro.core.Workspace;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.settings.PreferenceKeys;


public class WorskpaceAdapter extends ArrayAdapter<WorskpaceAdapter.WorkspaceViewModel> {

	Context context;
	int layoutResourceId;

	public WorskpaceAdapter(Context context, int resource, List<WorkspaceViewModel> objects) {
		super(context, resource, objects);
		this.layoutResourceId = resource;
		this.context = context;
	}

	public void refresh() {
		super.clear();

		Repository repo = Repository.open(context);
		
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());				
		int bankId = Integer.parseInt(prefs.getString(PreferenceKeys.PREFERENCE_BANK, "0"));

		List<Workspace> source = repo.getWorkspacesLight(bankId);

		for (Workspace workspace : source)
			super.add(new WorskpaceAdapter.WorkspaceViewModel(workspace));

		this.notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View row = convertView;
		WorkspaceViewModel workspace = super.getItem(position);

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(R.layout.list_item_current, parent, false);
		}

		TextView title = ((TextView) row.findViewById(R.id.title_text_view));

		title.setText(workspace.name);

		return row;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {

		View row = convertView;
		WorkspaceViewModel workspace = super.getItem(position);

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);
		}

		TextView title = ((TextView) row.findViewById(R.id.title_text_view));

		title.setText(workspace.name);

		return row;
	}

	public class WorkspaceViewModel {
		public int id;
		public String name;

		public WorkspaceViewModel(Workspace workspace) {

			id = workspace.getId();
			name = workspace.getName();
		}
	}
}
