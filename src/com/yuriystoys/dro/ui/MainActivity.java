/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.ui;

import java.util.HashMap;
import java.util.Map;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.MachineTypes;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.callbacks.IConnectionStateChangedCallback;
import com.yuriystoys.dro.callbacks.IReadoutCallback;
import com.yuriystoys.dro.callbacks.IToastCallback;
import com.yuriystoys.dro.core.BluetoothDroDriver;
import com.yuriystoys.dro.core.ConnectionState;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.DroDriver;
import com.yuriystoys.dro.core.UsbDroDriver;
import com.yuriystoys.dro.core.Workspace;
import com.yuriystoys.dro.data.Repository;
import com.yuriystoys.dro.settings.DroSettingsActivity;
import com.yuriystoys.dro.settings.PreferenceKeys;
import com.yuriystoys.dro.settings.StateKeys;

public class MainActivity extends FragmentActivity implements IToastCallback, IConnectionStateChangedCallback,
		ActionBar.OnNavigationListener {

	// Intent request codes
	private static final int REQUEST_ENABLE_BT = 1;

	private static final String FRAGMENT_TAG = "CURRENT_FRAGMENT";

	private MenuItem connectItem = null;
	private MenuItem disconnectItem = null;

	private final Map<String, Bundle> fragmentStates = new HashMap<String, Bundle>();

	@Override
	protected void onStart() {

		super.onStart();

		getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		// TODO: Create a dialog that does this.

		/*
		 * if (((DroApplication) getApplication()).getCurrentWorkspace() ==
		 * null) { Intent intent = new Intent(this,
		 * SelectWorkspaceActivity.class); startActivity(intent); }
		 */

		int driver = ((DroApplication) getApplication()).getDriver().getState();

		if (((DroApplication) getApplication()).getDriver() instanceof BluetoothDroDriver
				&& driver == ConnectionState.DISABLED) {
			Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		}

		// SharedPreferences sharedPrefs =
		// PreferenceManager.getDefaultSharedPreferences(this);
		// ((DroApplication)
		// getApplication()).getReadout().loadPreferences(sharedPrefs, this);
	}

	@Override
	protected void onResume() {
		super.onResume();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		final ActionBar actionBar = getActionBar();

		if (findViewById(R.id.tabs_fragment) == null) {
			// this is a [temporary] test for "small" layout

			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

			// Set up the dropdown list navigation in the action bar.
			actionBar.setListNavigationCallbacks(
			// Specify a SpinnerAdapter to populate the dropdown list.
					new ArrayAdapter<String>(getActionBarThemedContextCompat(), android.R.layout.simple_list_item_1,
							android.R.id.text1, getResources().getStringArray(R.array.tabs)), this);
		} else if (findViewById(R.id.tabs_fragment) != null && prefs.getBoolean(PreferenceKeys.DONT_USE_PANES, false)) {
			findViewById(R.id.tabs_fragment).setVisibility(View.GONE);

			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);

			// Set up the dropdown list navigation in the action bar.
			actionBar.setListNavigationCallbacks(
			// Specify a SpinnerAdapter to populate the dropdown list.
					new ArrayAdapter<String>(getActionBarThemedContextCompat(), android.R.layout.simple_list_item_1,
							android.R.id.text1, getResources().getStringArray(R.array.tabs)), this);

			onNavigationItemSelected(0, 0);
		} else {
			onNavigationItemSelected(0, 0);
		}

		Workspace workspace = ((DroApplication) getApplication()).getCurrentWorkspace();

		if (workspace == null) {

			int bankId = Integer.parseInt(prefs.getString(PreferenceKeys.PREFERENCE_BANK, "0"));

			Repository repo = Repository.open(this);
			workspace = repo.getLastWorkspace(bankId);

			if (workspace == null) {
				workspace = Workspace.create(this);
				workspace.setName(getString(R.string.default_workspace));
				repo.insertWorkspace(workspace);
			}

			((DroApplication) getApplication()).setCurrentWorkspace(workspace);

			if (findViewById(R.id.tabs_fragment) != null) {
				onNavigationItemSelected(0, 0);
			}
		}

		DroApplication app = (DroApplication) getApplication();
		// Dro dro = app.getReadout();

		if (prefs.getBoolean(PreferenceKeys.USE_USB, false) != app.usesUsb()) {
			// connection type changed so the app needs to restart
			Toast.makeText(this, "Connection settings changed. Restarting the application ...", Toast.LENGTH_LONG)
					.show();

			Intent i = getBaseContext().getPackageManager()
					.getLaunchIntentForPackage(getBaseContext().getPackageName());
			i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(i);
			System.exit(1);
		}

		// try to resume connection if needed (and appropriate)
		DroDriver driver = app.getDriver();

		if (prefs.getBoolean(StateKeys.LAST_CONNECTION_CONNECTED, false)) {
			boolean usedUsb = prefs.getBoolean(StateKeys.LAST_CONNECTION_USB, false);

			if (usedUsb == app.usesUsb()) // make sure the connection type
											// is still the same
			{
				if (usedUsb) {
					driver.connect(null);
				} else {
					String deviceName = prefs.getString(StateKeys.LAST_CONNECTION_BT_DEVICE, "");
					if (deviceName != null && deviceName.length() > 0) {
						((BluetoothDroDriver) driver).setLastUsedDevice(deviceName);
					}
				}
			}
		}

		driver.registerCallback((IToastCallback) this);
		driver.registerCallback((IConnectionStateChangedCallback) this);
		driver.registerCallback((IReadoutCallback) app.getDro());

		int state = driver.getState();

		this.onConnectionStateChanged(state);
	}

	@Override
	protected void onPause() {
		DroApplication app = (DroApplication) getApplication();

		// this should succeed, since the parent activity is checking for this
		Dro dro = app.getDro();

		DroDriver driver = app.getDriver();

		driver.removeCallback((IToastCallback) this);
		driver.removeCallback((IConnectionStateChangedCallback) this);

		driver.removeCallback((IReadoutCallback) dro);

		// save current connection state to preferences

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

		Editor editor = prefs.edit();

		editor.putBoolean(StateKeys.LAST_CONNECTION_USB, app.usesUsb());
		editor.putBoolean(StateKeys.LAST_CONNECTION_CONNECTED, driver.getState() == ConnectionState.CONNECTED);

		String btDevice = "";
		if (!app.usesUsb() && driver instanceof BluetoothDroDriver) {
			btDevice = ((BluetoothDroDriver) driver).getCurrentDeviceName();
		}

		editor.putString(StateKeys.LAST_CONNECTION_BT_DEVICE, btDevice);

		editor.commit();

		super.onPause();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

		Workspace workspace = ((DroApplication) getApplication()).getCurrentWorkspace();

		if (workspace == null) {

			SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			int bankId = Integer.parseInt(prefs.getString(PreferenceKeys.PREFERENCE_BANK, "0"));

			Repository repo = Repository.open(this);

			if (savedInstanceState != null) {
				int workspaceId = savedInstanceState.getInt(StateKeys.SELECTED_WORKSPACE, -1);
				workspace = repo.getWorkspace(workspaceId);

				if (workspace.getBankId() != bankId)
					workspace = repo.getLastWorkspace(bankId); // in case the
																// preference
																// bank has
																// changed

			} else {

				workspace = repo.getLastWorkspace(bankId);
			}

			if (workspace == null) {
				workspace = Workspace.create(this);
				workspace.setName(getString(R.string.default_workspace));
				repo.insertWorkspace(workspace);
			}

			((DroApplication) getApplication()).setCurrentWorkspace(workspace);
		}

		// set full screen mode
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_main);

		// Set up the action bar to show a dropdown list.
		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("");

		/*
		 * if (findViewById(R.id.point_list_fragment) == null) { // this is a
		 * [temporary] test for "small" layout
		 * 
		 * actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		 * 
		 * // Set up the dropdown list navigation in the action bar.
		 * actionBar.setListNavigationCallbacks( // Specify a SpinnerAdapter to
		 * populate the dropdown list. new
		 * ArrayAdapter<String>(getActionBarThemedContextCompat(),
		 * android.R.layout.simple_list_item_1, android.R.id.text1,
		 * getResources().getStringArray(R.array.tabs)), this); } else {
		 * onNavigationItemSelected(0, 0); }
		 */

	}

	/**
	 * Backward-compatible version of {@link ActionBar#getThemedContext()} that
	 * simply returns the {@link android.app.Activity} if
	 * <code>getThemedContext</code> is unavailable.
	 */
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	private Context getActionBarThemedContextCompat() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
			return getActionBar().getThemedContext();
		} else {
			return this;
		}
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		// Restore the previously serialized current dropdown position.
		if (savedInstanceState.containsKey(StateKeys.STATE_SELECTED_NAVIGATION_ITEM)) {
			getActionBar().setSelectedNavigationItem(
					savedInstanceState.getInt(StateKeys.STATE_SELECTED_NAVIGATION_ITEM));
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// Serialize the current dropdown position.
		outState.putInt(StateKeys.STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());

		Fragment fragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);

		if (fragment != null)
			fragment.onSaveInstanceState(outState);

		Workspace workspace = ((DroApplication) getApplication()).getCurrentWorkspace();

		if (workspace != null) {
			outState.putInt(StateKeys.SELECTED_WORKSPACE, workspace.getId());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);

		// cache menu items (to show/hide later in response to connections state
		// changes)
		this.connectItem = menu.findItem(R.id.menu_connect);
		this.disconnectItem = menu.findItem(R.id.menu_disconnect);

		return true;
	}

	/* Connection stuff */

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_connect:

			DroDriver driver = ((DroApplication) getApplication()).getDriver();

			if (driver instanceof UsbDroDriver) {
				try {
					driver.connect(null);
				} catch (IllegalStateException ex) {
					Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
				}
				return true;
			}

			ConnectDialog dialog = new ConnectDialog();
			dialog.show(getSupportFragmentManager(), "Dialog");

			return true;
		case R.id.menu_disconnect:

			((DroApplication) getApplication()).getDriver().disconnect();

			return true;
		case R.id.menu_settings:

			Intent intent = new Intent(this, DroSettingsActivity.class);
			startActivity(intent);
			return true;

		case R.id.menu_tool_library: {

			MachineTypes mt = ((DroApplication) getApplication()).getDro().getMachineType();

			switch (mt) {
			case VERTICAL_MILL: {
				MillToolEditDialog.Show(MainActivity.this);
				break;
			}
			case HORIZONTAL_MILL: {
				MillToolEditDialog.Show(MainActivity.this);
				break;
			}
			case LATHE: {
				LatheToolEditDialog.Show(MainActivity.this);
				break;
			}
			}

			// Intent intent2 = new Intent(this, DroSettingsActivity.class);
			// startActivity(intent2);
			return true;
		}
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void MakeToast(String message) {
		Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onBackPressed() {
		if (!goToReadoutScreen()) {
			super.onBackPressed();
		}
	}

	public boolean goToReadoutScreen() {
		if (navPosition > 0) {
			getActionBar().setSelectedNavigationItem(0);
			return true;
		}
		return false;
	}

	int navPosition = 0;

	@Override
	public boolean onNavigationItemSelected(int position, long id) {

		navPosition = position;

		// If there is a current fragment, save it's state to the map

		Fragment oldFragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_TAG);
		Fragment newFragment = null;
		String newClassName = null;

		if (oldFragment != null) {
			String className = oldFragment.getClass().getSimpleName();
			Bundle bundle = new Bundle();

			oldFragment.onSaveInstanceState(bundle);

			fragmentStates.put(className, bundle);
		}

		// determine what was clicked and switch to that fragment
		switch (position) {
		case 0: {

			// if there is a state bundle, pass it to the fragment
			newClassName = DroDisplayFragment.class.getSimpleName();

			if (fragmentStates.containsKey(newClassName)) {
				// pass the sate to the constructor
				newFragment = new DroDisplayFragment();
				newFragment.setArguments(fragmentStates.get(newClassName));
			} else {
				// no state, so pass null
				newFragment = new DroDisplayFragment();
			}

			// replace the fragment

			getSupportFragmentManager().beginTransaction().replace(R.id.container, newFragment, FRAGMENT_TAG).commit();

			break;
		}
		case 1: {
			newFragment = new PointListFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.container, newFragment, FRAGMENT_TAG).commit();
			break;
		}
		case 2: {
			newFragment = new ToolListFragment();
			getSupportFragmentManager().beginTransaction().replace(R.id.container, newFragment, FRAGMENT_TAG).commit();
			break;
		}
		}

		return true;
	}

	@Override
	public void onConnectionStateChanged(int status) {
		ActionBar bar = this.getActionBar();

		int actionBarTitleId = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTextView = (TextView) findViewById(actionBarTitleId);
		boolean hideConnect = false;

		switch (status) {
		case ConnectionState.CONNECTED:

			bar.setTitle("DRO - Connected");
			hideConnect = true;

			actionBarTextView.setTextColor(getResources().getColor(android.R.color.holo_green_light));

			break;
		case ConnectionState.CONNECTING:

			bar.setTitle("DRO - Connecting");
			hideConnect = true;

			actionBarTextView.setTextColor(getResources().getColor(android.R.color.holo_green_dark));

			break;
		case ConnectionState.CONNECTION_LOST:
			bar.setTitle("DRO - Connection lost");
			hideConnect = true;

			actionBarTextView.setTextColor(getResources().getColor(android.R.color.holo_orange_light));

			break;
		case ConnectionState.DISABLED:
		case ConnectionState.DISCONNECTED:
		default:

			bar.setTitle("DRO - Disconnected");
			hideConnect = false;

			actionBarTextView.setTextColor(getResources().getColor(android.R.color.white));

			break;
		}

		if (this.connectItem != null)
			connectItem.setVisible(!hideConnect);

		if (this.disconnectItem != null)
			disconnectItem.setVisible(hideConnect);
	}

}
