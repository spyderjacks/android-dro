/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */




public final class PreferenceKeys {

	private PreferenceKeys() {
	}
	
	public final static String CONNECT_AUTOMATICALLY = "connect_automatically";

	public final static String METRIC_MODE = "metric_mode";

	public final static String MACHINE_TYPE = "machine_type";
	public final static String PREFERENCE_BANK = "preference_bank";
	public final static String PREFERENCE_BANK_NAME = "preference_bank_name";

	public final static String DISPLAY_FORMAT = "display_format";
	public final static String DISPLAY_FONT = "display_font";

	public final static String X_LABEL = "x_label";
	public final static String X_TPI = "x_tpi";
	public final static String X_INVERT = "x_invert";
	public final static String X_ENABLE = "x_enable";
	public final static String X_FILTER = "x_filter";

	public final static String Y_LABEL = "y_label";
	public final static String Y_TPI = "y_tpi";
	public final static String Y_INVERT = "y_invert";
	public final static String Y_ENABLE = "y_enable";
	public final static String Y_FILTER = "y_filter";

	public final static String Z_LABEL = "z_label";
	public final static String Z_TPI = "z_tpi";
	public final static String Z_INVERT = "z_invert";
	public final static String Z_ENABLE = "z_enable";
	public final static String Z_FILTER = "z_filter";

	public final static String W_LABEL = "w_label";
	public final static String W_TPI = "w_tpi";
	public final static String W_INVERT = "w_invert";
	public final static String W_ENABLE = "w_enable";
	public final static String W_TARGET = "w_target";
	public final static String W_FILTER = "w_filter";

	public final static String TACH_LABEL = "tach_label";
	public final static String TACH_PPR = "tach_ppr";
	public final static String TACH_INVERT = "tach_invert";
	public final static String TACH_ENABLE = "tach_enable";
	public final static String TACH_FILTER = "tach_filter";

	public final static String WORKSPACE_ID = "workspace_id";

	public final static String USE_USB = "use_usb";
	public final static String USB_BAUD_RATE = "usb_baud_rate";
	
	public final static String USE_METRIC_MODE = "use_metric_mode";
	
	public final static String USE_TABLET_LAYOUT = "use_tablet_layout";
	public final static String DONT_USE_PANES = "dont_use_panes";
	
	public final static String DONT_SHOW_SETUP_REMINDER = "dont_show_setup_reminder";
	public final static String SETUP_COMPLETED = "dont_show_setup_reminder";

	public enum DisplayFonts {
		SEVEN_SEGMENT, MONOSPACE, SYSTEM
	}
}
